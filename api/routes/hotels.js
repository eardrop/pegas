const { Router } = require('express')
const request = require('request')
const router = Router()

router.get('/hotels', (req, res, next) => {
  request.get({
    url: 'http://hotels-admin.pegast.su/api/v1' + req.url
  }, (error, response, body) => {
    console.log(error)
    res.send(body)
  })
})

router.get('/hotel/:id', (req, res, next) => {
  request.get({
    url: 'http://hotels-admin.pegast.su/api/v1/hotels/' + req.params.id
  }, (error, response, body) => {
    console.log(error)
    res.send(body)
  })
})

module.exports = router
