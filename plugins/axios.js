import * as axios from 'axios'

let request = () => {
  let call
  return (type, url) => {
    if (process.title === 'browser') {
      document.getElementById('loader').className = 'loader-show'
    }
    if (call) {
      call.cancel()
    }
    call = axios.CancelToken.source()
    return axios[type](url, { cancelToken: call.token }).then((response) => {
      if (process.title === 'browser') {
        document.getElementById('loader').className = ''
      }
      return response
    }).catch(thrown => {
      if (axios.isCancel(thrown)) {
        console.log('First request canceled')
      }
      return {
        data: {
          error: true
        }
      }
    })
  }
}
let call = request()

export default {
  request: (type, url) => {
    return call(type, url)
  }
}
